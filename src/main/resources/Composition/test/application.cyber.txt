CREATE 
	(Amazon:CSP {name:'Amazon', app_id:'1'}),
	(vm:IaaS:service {name:'vm', app_id:'1'}),
	(webapp:SaaS:service {name:'webapp', type:'WEB', app_id:'1', component_id:'1'}),
	(db:SaaS:service {name:'db', type:'STORAGE', app_id:'1', component_id:'2'}),
	(Amazon) -[:provides {name:'provides'}]->(vm),
	(vm)-[:hosts {name:'hosts'}]->(webapp),
	(vm)-[:hosts {name:'hosts'}]->(db),
	(webapp)-[:uses {name:'uses'}]->(db)