package eu.musaproject.multicloud.engines.composition;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.models.macmSLA.SLAPeerType;
import eu.musaproject.multicloud.models.macmSLA.SLARelationshipType;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.multicloud.models.macmSLA.macmSLAT;
import eu.musaproject.multicloud.utilities.NeoDriver;

public abstract class SLAcomposer {
	public MACM mcapp;
	protected NeoDriver neo;
	protected SLAreasoner slar;
	protected List<macmSLA> slas;

	public List<macmSLA> getSlas() {
		return slas;
	}

	public SLAcomposer() throws SQLException {
		mcapp=new MACM();
		neo= new NeoDriver();
		slar=new SLAreasoner(mcapp);
	}

	public abstract void setMACMinNeo();
	
	public abstract void setSLATinNeo(String provider);
	
	public abstract void setSLAinNeo(String provider);
	
	public abstract macmSLA getSLAfromNeo(String provider);

	public abstract macmSLAT getSLATfromNeo(String provider);

	public void cleanMACM(String mcappid) {
		String statement="match (n {app_id:'"+mcappid+"'}) detach delete(n)";
		neo.execute(statement);
	}
	
	public void cleanMACMslas(String mcappid) {
		String statement="match (n:SecurityMetric {app_id:'"+mcappid+"'}) detach delete(n)";
		neo.execute(statement);
		statement="match (n:SLO {app_id:'"+mcappid+"'}) detach delete(n)";
		neo.execute(statement);
		statement="match (n:SecurityControl {app_id:'"+mcappid+"'}) detach delete(n)";
		neo.execute(statement);
		statement="match (n:ControlFamily {app_id:'"+mcappid+"'}) detach delete(n)";
		neo.execute(statement);
		statement="match (n:SLA {app_id:'"+mcappid+"'}) detach delete(n)";
		neo.execute(statement);
		statement="match (n:SLAT {app_id:'"+mcappid+"'}) detach delete(n)";
		neo.execute(statement);

	}
	
	public List<String> Services() {
		List<String> services=new ArrayList<String>();
		Map <String,Peer> map= mcapp.getPeers();
		Set<String> keys=map.keySet();
		String keystring[]= new String[keys.size()];
		Iterator<String> iter=keys.iterator();
		int i=0;
		while (iter.hasNext()) {
			keystring[i]=new String((String) iter.next());
			i++;
		}
		Peer tmpPeer;
		for (i=0;i<keystring.length;i++) {
			tmpPeer=map.get(keystring[i]);
			if (tmpPeer.type==macmPeerType.IaaS) {
				services.add(tmpPeer.getName());
			} else if (tmpPeer.type==macmPeerType.PaaS) {
				services.add(tmpPeer.getName());
			} else if (tmpPeer.type==macmPeerType.SaaS) {
				services.add(tmpPeer.getName());
			}
		}		
		return services;		
	}
	
	
	public List<Peer> Services_peer() {
		List<Peer> services=new ArrayList<Peer>();
		Map <String,Peer> map= mcapp.getPeers();
		Set<String> keys=map.keySet();
		String keystring[]= new String[keys.size()];
		Iterator<String> iter=keys.iterator();
		int i=0;
		while (iter.hasNext()) {
			keystring[i]=new String((String) iter.next());
			i++;
		}
		Peer tmpPeer;
		for (i=0;i<keystring.length;i++) {
			tmpPeer=map.get(keystring[i]);
			if (tmpPeer.type==macmPeerType.IaaS) {
				services.add(tmpPeer);
			} else if (tmpPeer.type==macmPeerType.PaaS) {
				services.add(tmpPeer);
			} else if (tmpPeer.type==macmPeerType.SaaS) {
				services.add(tmpPeer);
			}
		}		
		return services;		
	}
	
	public List<String> CSPs() {
		List<String> csps=new ArrayList<String>();
		Map <String,Peer> map= mcapp.getPeers();
		Set<String> keys=map.keySet();
		String keystring[]= new String[keys.size()];
		Iterator<String> iter=keys.iterator();
		int i=0;
		while (iter.hasNext()) {
			keystring[i]=new String((String) iter.next());
			i++;
		}
		Peer tmpPeer;
		for (i=0;i<keystring.length;i++) {
			tmpPeer=map.get(keystring[i]);
			if (tmpPeer.type==macmPeerType.CSP) {
				csps.add(tmpPeer.getName());
			} 
		}		
		return csps;		
	}
	
	public List<Peer> CSPs_peer() {
		List<Peer> csps=new ArrayList<Peer>();
		Map <String,Peer> map= mcapp.getPeers();
		Set<String> keys=map.keySet();
		String keystring[]= new String[keys.size()];
		Iterator<String> iter=keys.iterator();
		int i=0;
		while (iter.hasNext()) {
			keystring[i]=new String((String) iter.next());
			i++;
		}
		Peer tmpPeer;
		for (i=0;i<keystring.length;i++) {
			tmpPeer=map.get(keystring[i]);
			if (tmpPeer.type==macmPeerType.CSP) {
				csps.add(tmpPeer);
			} 
		}		
		return csps;		
	}
	public void createslas() {
		Map <String,Peer> map= mcapp.getPeers();
		Set<String> keys=map.keySet();
		String keystring[]= new String[keys.size()];
		Iterator<String> iter=keys.iterator();
		int i=0;
		while (iter.hasNext()) {
			keystring[i]=new String((String) iter.next());
			i++;
		}
		Peer tmpPeer;
		slas=new ArrayList<macmSLA>();
		for (i=0;i<keystring.length;i++) {
			tmpPeer=map.get(keystring[i]);
			if (tmpPeer.type==macmPeerType.CSP) {
				slas.add(getSLAfromNeo(tmpPeer.getName()));
			} else if (tmpPeer.type==macmPeerType.IaaS) {
				slas.add(getSLAfromNeo(tmpPeer.getName()));
				slas.add(getSLATfromNeo(tmpPeer.getName()));
			} else if (tmpPeer.type==macmPeerType.PaaS) {
				slas.add(getSLAfromNeo(tmpPeer.getName()));
				slas.add(getSLATfromNeo(tmpPeer.getName()));
			} else if (tmpPeer.type==macmPeerType.SaaS) {
				slas.add(getSLAfromNeo(tmpPeer.getName()));
				slas.add(getSLATfromNeo(tmpPeer.getName()));
			}
		}	
	}
	
	public void updateslas() {
		if (slas==null) createslas();
	}

	public void prepare() {
		slar.evaluateCompositionRulesByFamily("AC");
		slar.evaluateCompositionRulesByFamily("PE");
		slar.evaluateCompositionRulesByFamily("IA");
		slar.evaluateCompositionRulesByFamily("SI");
		slar.evaluateCompositionRulesByFamily("RA");
		slar.evaluateCompositionRulesByFamily("SA");
		slar.evaluateCompositionRulesByFamily("SC");
		slar.evaluateCompositionRulesByFamily("IR");
		slar.evaluateCompositionRulesByFamily("MP");
		slar.evaluateCompositionRulesByFamily("CM");
		slar.evaluateCompositionRulesByFamily("CA");
		slar.evaluateCompositionRulesByFamily("AT");
	}

	public macmSLA ComposedSLAinNeo(String provider, String slaname) {
		System.out.println("Querying for "+provider);
		macmSLA sla=slar.getSLA2(provider);
		
		//Add SLOs and metrics from SLAT
		macmSLAT slat=getSLATfromNeo(provider);
		Map<String,Peer> slatpeers=slat.getPeers();
		List<Relationship> slatrels=slat.getRelationship();
		Peer tmpPeer;

		
		//Add Metrics		
		for(Map.Entry<String,Peer> entry : slatpeers.entrySet()) {
			if (entry.getValue().getType()==SLAPeerType.SecurityMetric) {
				sla.addMetric(entry.getValue().getName());
			}
		}
		
		//Add SLOs
		for(Map.Entry<String,Peer> entry : slatpeers.entrySet()) {
			if (entry.getValue().getType()==SLAPeerType.SLO) {
				tmpPeer=sla.addSLO(entry.getValue().getName());
				sla.addSLOin(entry.getValue().getName(), sla.getName());
				for (int i=0;i<slatrels.size();i++) {
					if ((slatrels.get(i).getStartNode().getName().equals(entry.getValue().getName()))&&(slatrels.get(i).getType()==SLARelationshipType.MeasuredWith)) {
						sla.addMeasuredWith(entry.getValue().getName(),slatrels.get(i).endNode.getName());
					}
					 	
				}
				
			}
		}

		
		sla.setAppid(mcapp.getAppid());
		sla.setSlaid("sla"+provider+"-"+mcapp.getAppid());
		sla.writeNeo();
		mcapp.addSLA(slaname);
		Relationship r=mcapp.addGrants(provider, slaname);
		mcapp.syncNeoRelationship(r);
		return sla;
	}

	public void compose() {
		Map <String,Peer> map= mcapp.getPeers();
		String tmp;
		System.out.print("Start Composing ");
		Set<String> keys=map.keySet();
		String keystring[]= new String[keys.size()];
		Iterator<String> iter=keys.iterator();
		int i=0;
		while (iter.hasNext()) {
			keystring[i]=new String((String) iter.next());
			i++;
		}
		Peer tmpPeer;
		//Compose IaaS
		for (i=0;i<keystring.length;i++) {
			tmpPeer=map.get(keystring[i]);
			if (tmpPeer.type==macmPeerType.IaaS) {
				tmp=tmpPeer.getName();
				ComposedSLAinNeo(tmp,tmp+"_sla");
			}
		}
		//Compose PaaS
		for (i=0;i<keystring.length;i++) {
			tmpPeer=map.get(keystring[i]);
			if (tmpPeer.type==macmPeerType.PaaS) {
				tmp=tmpPeer.getName();
				ComposedSLAinNeo(tmp,tmp+"_sla");
			} 
		}
		
		//Compose SaaS
		for (i=0;i<keystring.length;i++) {
			tmpPeer=map.get(keystring[i]);
			if (tmpPeer.type==macmPeerType.SaaS) {
				tmp=tmpPeer.getName();
				ComposedSLAinNeo(tmp,tmp+"_sla");
			}
		}
	}


//	public void session() throws IOException, SQLException {
//		int i;
//		neo.cleanDB();
//		
//		System.out.println("\n***Setup MACM***\n");
//		setMACMinNeo();
//		mcapp.readNeo();
//		List<String> services=Services();
//		List<String> csps=CSPs();
//		
//		//Add the SLAs for the CSPs
//		System.out.println("\n**Setup CSP SLAs**\n");		
//		for (i=0;i<csps.size();i++){
//			System.out.println("Adding:"+services.get(i));
//			setSLAinNeo(csps.get(i));
//		}
//
//		System.out.println("\n**Setup Services SLATs**\n");		
//		//Add the SLATs for the services
//		for (i=0;i<services.size();i++){
//			System.out.println("Adding:"+services.get(i));
//			setSLATinNeo(services.get(i));
//		}
//
//		System.out.println("Prepare composition");		
//		prepare();
////		Scanner scan=new Scanner(System.in);
////		System.out.println("Press Enter to Compose");
////		scan.nextLine();				
////
//		System.out.println("Compose");		
//		compose();	
//		
////		//Retrieve the SLAs for the services
//////		macmSLA tmp;
//////		for (i=0;i<services.size();i++){
//////			tmp=getSLAfromNeo(services.get(i));
//////			slas[i]=tmp;
//////		}
////		
//		System.out.println("Create SLAs");
//		createslas();
////		NISTrules nist=new NISTrules();
////		nist.tablePrint(slas);
//	}

	public void session() throws IOException, SQLException {		
		System.out.println("\n*** Setup MACM and SLAs ***\n");
		setup();
		System.out.println("\n*** Composition ***\n");		
		DoCompose();
//		System.out.println("\n*** Complete Process ***\n");		
//		complete();
	}

	public void setup() throws IOException {
		int i;
		neo.cleanDB();
		setMACMinNeo();
		mcapp.readNeo();
		List<String> services=Services();
		List<String> csps=CSPs();
		
		//Add the SLAs for the CSPs
		for (i=0;i<csps.size();i++){
			setSLAinNeo(csps.get(i));
		}
		
		//Add the SLATs for the services
		for (i=0;i<services.size();i++){
			setSLATinNeo(services.get(i));
		}
	}
	
	public void DoCompose() {
		prepare();
		compose();
	}
	
	public void complete() throws FileNotFoundException, SQLException {
		createslas();
		NISTrules nist=new NISTrules();
		nist.tablePrint(slas);
	}
	
}
