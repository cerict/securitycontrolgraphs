package eu.musaproject.multicloud.scenarios;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import eu.musaproject.multicloud.converters.caiqTree2macmSLA;
import eu.musaproject.multicloud.converters.ccm2nist;
import eu.musaproject.multicloud.converters.convertException;
import eu.musaproject.multicloud.converters.macm2wsag;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;

public class AmzonSLA {
	public static  void main(String args[]) {
		try {
			MACM macm=new MACM();
			macm.addCSP("AmazonEC2");
			macm.addSLA("AmazonEC2_sla");
			macm.addGrants("AmazonEC2", "AmazonEC2_sla");
			macm.writeNeo();
			String file="src/main/resources/SLAs/AmazonEC2-CAIQ-v2.xml";
			caiqTree2macmSLA c2w=new caiqTree2macmSLA(file);
			macmSLA sla=c2w.readsla();
			//sla.print();
			ccm2nist c=new ccm2nist();
			c.setupMap();
			macmSLA nistSLA=c.getNIST(sla);
			nistSLA.writeNeo();		
			String a=macm2wsag.macm2wsagString(macm,"AmazonEC2");
//			System.out.println(a);
			Writer writer = null;
			String outputfile="src/main/resources/SLAs/AmazonEc2NIST.xml";
			try {
			    writer = new BufferedWriter(new OutputStreamWriter(
			          new FileOutputStream(outputfile), "utf-8"));
			    writer.write(a);
			} catch (IOException ex) {
			  // report
			} finally {
			   try {writer.close();} catch (Exception ex) {/*ignore*/}
			}
		} catch (convertException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
