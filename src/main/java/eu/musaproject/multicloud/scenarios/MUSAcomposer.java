package eu.musaproject.multicloud.scenarios;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import eu.musaproject.multicloud.converters.wsag2neo;
import eu.musaproject.multicloud.converters.macm2wsag;
import eu.musaproject.multicloud.engines.composition.NISTrules;
import eu.musaproject.multicloud.engines.composition.SLAcomposer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.multicloud.models.macmSLA.macmSLAT;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class MUSAcomposer extends SLAcomposer {

	String SLApath;
	String SLATpath;
	String ComposedSLApath;
	String MACMpath;
	String MACMfile;
	String mcappid;
	boolean SLApathStateAvailable=false;
	boolean SLATpathStateAvailable=false;
	boolean MACMfileStateAvailable=false;
	boolean ComposedSLApathStateAvailable=false;
	
	public MUSAcomposer() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}


	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
	
	@Override
	public void setMACMinNeo() {
		if (!MACMfileStateAvailable) {
			System.out.println("MACM not set!");
			System.exit(-1);
		}
		try {
			cleanMACM(mcappid);
			neo.executeFromFile(MACMfile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setMACMinNeo(String mcappid) {
		try {
			cleanMACM(mcappid);
			neo.executeFromFile(MACMpath+mcappid+".macm");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void setSLATinNeo(String provider) {
		// TODO Auto-generated method stub
		if (!SLATpathStateAvailable) {
			System.out.println("SLAT path not set!");
			System.exit(-1);
		}
		
		String slatname=provider+"_slat";
		String wsag;
		try {
			wsag = readFile(SLATpath+slatname+".xml"
					,StandardCharsets.UTF_8);
			wsag2neo.wsagGraph(wsag,"SLAT",slatname,mcapp.getAppid());
			mcapp.addSLAT(slatname);
			Relationship r=mcapp.addSupports(provider, slatname);
			mcapp.syncNeoRelationship(r);
			macmSLAT slat=new macmSLAT(provider+"_slat",Integer.valueOf(mcappid));
			slat.readNeo(slatname);
			slar.addSLATtoKB(slat);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void setSLAinNeo(String provider) {
		// TODO Auto-generated method stub
		if (!SLApathStateAvailable) {
			System.out.println("SLA path not set!");
			System.exit(-1);
		}
		
		String slaname=provider+"_sla";
		String wsag;
		try {
			wsag = readFile(SLApath+slaname+".xml",StandardCharsets.UTF_8);
			wsag2neo.wsagGraph(wsag,"SLA",slaname,mcapp.getAppid());
			mcapp.addSLA(slaname);
			Relationship r=mcapp.addGrants(provider, slaname);
			mcapp.syncNeoRelationship(r);
			macmSLA sla=new macmSLA();
			sla.readNeo(slaname);
			slar.addSLAtoKB(sla);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public macmSLA getSLAfromNeo(String provider) {
		// TODO Auto-generated method stub
		String slaname=provider+"_sla";
		macmSLA sla= new macmSLA();
		sla.readNeo(slaname);
		return sla;
	}

	@Override
	public macmSLAT getSLATfromNeo(String provider) {
		// TODO Auto-generated method stub
		String slatname=provider+"_slat";
		macmSLAT slat= new macmSLAT();
		slat.setAppid(mcapp.getAppid());
		slat.readNeo(slatname);
		return slat;
	}

	
	public void setup() throws IOException {
		int i;
		setMACMinNeo(mcappid);
		mcapp.setAppid(mcappid);
		mcapp.readNeo(mcappid);
		
		List<String> services=Services();
		List<String> csps=CSPs();
		
		//Add the SLAs for the CSPs
		for (i=0;i<csps.size();i++){
			setSLAinNeo(csps.get(i));
		}
		
		//Add the SLATs for the services
		for (i=0;i<services.size();i++){
			setSLATinNeo(services.get(i));
		}
	}
	
	public void complete() throws FileNotFoundException, SQLException {
		createslas();
		NISTrules nist=new NISTrules();
//		nist.tablePrint(slas);
	}
	
	public void configure(String propertyfile) {
		Properties pfile=new Properties();
		try {
			pfile.load(new FileReader(propertyfile));
			SLApath=pfile.getProperty("SLAPath");
			SLApathStateAvailable=true;
			SLATpath=pfile.getProperty("SLATPath");
			SLATpathStateAvailable=true;
			ComposedSLApath=pfile.getProperty("ComposedSLAPath");
			ComposedSLApathStateAvailable=true;
			MACMpath=System.getProperty("user.dir")+"/"+pfile.getProperty("MACMPath");
			MACMfile=pfile.getProperty("MACM");
			MACMfileStateAvailable=true;
			mcappid=pfile.getProperty("mcappid");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void printSLAs() {
		List<String> services=Services();
		macmSLA tmpSLA;
		int i;
		//Add the SLATs for the services
		for (i=0;i<services.size();i++){
			tmpSLA=getSLAfromNeo(services.get(i));
			System.out.println("*************");
			System.out.println("**"+tmpSLA.getName()+"**");		
			tmpSLA.printSecurityControls();
		}
	}

	public void generateComposedWSAGs() throws SQLException, FileNotFoundException, UnsupportedEncodingException {
		//Add the SLATs for the services
		List<String> services=Services();
		int i;
		//Add the SLATs for the services
		for (i=0;i<services.size();i++){
			String service=services.get(i);
			String file=ComposedSLApath+service+"_sla.xml";
			System.out.println("Print to file:"+file);
			macm2wsag converter=new macm2wsag();
			converter.macm2wsagFile(mcapp,services.get(i),file);
		}
	}
		
	public static void main(String args[]){
		String confName="src/main/resources/MUSA/ACMtops.properties";
		if (args.length>0) confName=args[0];
		try {
		 MUSAcomposer comp=new MUSAcomposer();
			comp.configure(confName);
			comp.setup();
			comp.mcapp.print();
			System.out.println("Press return to Prepare Compose");
			Scanner scan = new Scanner(System.in);
			String query = scan.nextLine();
			comp.prepare();
			System.out.println("Press return to Compose");
			scan = new Scanner(System.in);
			query = scan.nextLine();
			comp.compose();	
			System.out.println("Press return to Print SLAs");
			scan = new Scanner(System.in);
			query = scan.nextLine();
			comp.generateComposedWSAGs();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
