package eu.musaproject.multicloud.utilities;

import java.io.IOException;
import java.sql.SQLException;

import eu.musaproject.multicloud.scenarios.MUSAcomposer;

public class MUSAtester {

	static public void main(String args[]) {
		try {
			MUSAcomposer comp=new MUSAcomposer();
			NeoDriver neo= new NeoDriver();				
			neo.cleanDB();
			comp.configure("src/main/resources/MUSA/TUT.properties");
			comp.setMACMinNeo("211");
			comp.setMACMinNeo("311");
			comp.configure("src/main/resources/MUSA/LHS.properties");
			comp.setMACMinNeo("535");
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
