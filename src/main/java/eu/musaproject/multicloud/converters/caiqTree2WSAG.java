package eu.musaproject.multicloud.converters;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class caiqTree2WSAG {

	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}


	
	public void ReadCaiq(String filename) throws IOException, ParserConfigurationException, SAXException{
		String caiq=readFile(filename,StandardCharsets.UTF_8);
		DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();

		Document doc = dBuilder.parse(filename);
		NodeList nodes=doc.getChildNodes().item(0).getChildNodes();
		Node root=nodes.item(1);
		System.out.print("Root element: " + root.getNodeName()+" "+nodes.getLength());
		NamedNodeMap rootAtts=root.getAttributes();
		System.out.println(rootAtts.item(0));
		
	}
	
	
	public void ReadBase(String filename) throws IOException, ParserConfigurationException, SAXException{
		String caiq=readFile(filename,StandardCharsets.UTF_8);
		DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();

		Document doc = dBuilder.parse(filename);
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		if (doc.hasChildNodes()) {
			printNote(doc.getChildNodes());
		}
	}

	private static void printNote(NodeList nodeList) {

		for (int count = 0; count < nodeList.getLength(); count++) {
			Node tempNode = nodeList.item(count);

			// make sure it's element node.
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

				// get node name and value
				System.out.print("\nNode Name =" + tempNode.getNodeName() + " [OPEN] ");
				System.out.print("Node Value =" + tempNode.getTextContent());
				if (tempNode.hasAttributes()) {
					// get attributes names and values
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i = 0; i < nodeMap.getLength(); i++) {
						Node node = nodeMap.item(i);
						System.out.print ("attr name : " + node.getNodeName());
						System.out.print ("attr value : " + node.getNodeValue().trim());
					}
				}
				if (tempNode.hasChildNodes()) {
					// loop again if has child nodes
					printNote(tempNode.getChildNodes());
				}
				System.out.println("Node Name =" + tempNode.getNodeName() + " [CLOSE]");
			}
		}

	}
	public static void main(String args[]) throws ParserConfigurationException, SAXException {
		try {
			String file="src/main/resources/SLAs/AmazonEC2-CAIQ-v2.xml";
			caiqTree2WSAG c2w=new caiqTree2WSAG();
			c2w.ReadCaiq(file);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
}
