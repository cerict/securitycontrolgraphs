package eu.musaproject.multicloud.converters;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import eu.musaproject.multicloud.models.macm.MACM;

@SuppressWarnings("restriction")
public class camel2neo {


	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	public static MACM camel2graph(String camelFile) {

		MACM mcapp=new MACM();
		String camelString=null;
		Map <String,String > comms= new HashMap <String,String>();

		try {
			camelString=readFile(camelFile,StandardCharsets.UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		StringTokenizer camelTokens = new StringTokenizer(camelString);
		int state=0;
		int count=0;
	
		while (camelTokens.hasMoreTokens()) {
			count++;
			//search for components
			String token=camelTokens.nextToken();
			if (state==1) System.out.println("State:"+state+" "+token);
			if (state==2) {
				System.out.println("count:"+count+"State:"+state+" "+token);
				mcapp.addSaaService(token);
				int compstate=0;
				boolean inside=true;
				token=camelTokens.nextToken();
				while (inside) {
					//setup communications
				}
				state=0;
			}else if (state==1) {
				if (token.equalsIgnoreCase("component")) {
					state=2;
				} else
					state=0;
			} else if (state==0) {
				if (token.equalsIgnoreCase("internal")) {
					System.out.println("GOT IT");
					state=1;
				}
			} 
		}
		return mcapp;
		
	}

	public static void camel2neo(String camelFile) {
		MACM mcapp = camel2graph(camelFile);
		mcapp.writeNeo();
	}
	public static void main(String args[]) {
		camel2neo.camel2neo("src/main/resources/Models/tut.camel");
	}
}
