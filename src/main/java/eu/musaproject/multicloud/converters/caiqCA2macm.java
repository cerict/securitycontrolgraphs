package eu.musaproject.multicloud.converters;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.List;

import com.google.gson.Gson;

import eu.musaproject.multicloud.models.data.caiqCA.Metric;
import eu.musaproject.multicloud.models.data.caiqCA.caiqCA;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;

public class caiqCA2macm {

	public static caiqCA readFromFile(String file) {
        Gson gson = new Gson();
        caiqCA caiq = null;
        try (Reader reader = new FileReader(file)) {

			// Convert JSON to Java Object
           caiq = gson.fromJson(reader, caiqCA.class);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
		return caiq;
    }

	public static macmSLA createSLAFromCaiq(caiqCA caiq) {
		macmSLA sla=new macmSLA(caiq.getCsp().getName()+"_sla");
		List<Metric> metrics=caiq.getMetrics();
		Metric tmp=null;
		String name;
		int index=-1;
		for (int i=0;i<metrics.size();i++) {
			tmp=metrics.get(i);
			System.out.print(tmp.getSlaId()+" ");
			index=tmp.getSlaId().indexOf('.');
			if (index>0)
				name=tmp.getSlaId().substring(0,index);
			else name=name=tmp.getSlaId();
			name=name.replace('-', '_');
			System.out.print(name+" ");
			if (tmp.getValue()!=0.99) {
				sla.addSecurityControl(name);
				System.out.print("add ");
			} 
			System.out.println();
		}
		return sla;
	}
	
	public static MACM createMACMforSLA (macmSLA sla,caiqCA caiq) {
		MACM macm=new MACM();
		macm.addCSP(caiq.getCsp().getName());
		macm.addSLA(sla.getName());
		macm.addGrants(caiq.getCsp().getName(),sla.getName());
		return macm;
	}
	
	public static String caiqCA2wsag(caiqCA caiq) {
		macmSLA ccmSLA=createSLAFromCaiq(caiq);
		ccmSLA.print();
		ccm2nist converter=new ccm2nist();
		converter.setupMap();
		converter.SetCCM(ccmSLA);
		macmSLA nistSLA=converter.getNIST();
		MACM macm=createMACMforSLA(nistSLA,caiq);
		nistSLA.writeNeo();
		macm.writeNeo();
		String wsag=macm2wsag.macm2wsagString(macm,caiq.getCsp().getName());
		return wsag;
	}
	
    public static void main(String[] args) throws IOException {
    		caiqCA caiq= readFromFile("src/main/resources/SLAs/Aimes.ccm.json");
    		System.out.println("caiq for"+caiq.getCsp().getName());
    		String wsag=caiqCA2wsag(caiq);
    		Writer out=new FileWriter("src/main/resources/SLAs/Aimes.xml");
    		out.write(wsag);
    }
}
