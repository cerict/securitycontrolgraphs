package eu.musaproject.multicloud.models.macm;

public enum PeerType {
	CSP,
	CSC,
	party,
	IaaS,
	PaaS,
	SaaS,
	service,
	SLA,
	SLAT
}
