package eu.musaproject.multicloud.models.data.caiqCA;

public class Metric {
    int Id;
    String SlaId;
    String Question;
    String Answer;
    float Value;
    int ProviderId;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getSlaId() {
		return SlaId;
	}
	public void setSlaId(String slaId) {
		SlaId = slaId;
	}
	public String getQuestion() {
		return Question;
	}
	public void setQuestion(String question) {
		Question = question;
	}
	public String getAnswer() {
		return Answer;
	}
	public void setAnswer(String answer) {
		Answer = answer;
	}
	public float getValue() {
		return Value;
	}
	public void setValue(float value) {
		Value = value;
	}
	public int getProviderId() {
		return ProviderId;
	}
	public void setProviderId(int providerId) {
		ProviderId = providerId;
	}
    
}
