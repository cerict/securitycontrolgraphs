package eu.musaproject.multicloud.models.data.caiqCA;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import com.google.gson.Gson;

public class caiqCA {
	String Name;
	CSP Provider;
	List<Metric> Metrics;
	public CSP getCsp() {
		return Provider;
	}
	public void setCsp(CSP Provider) {
		this.Provider = Provider;
	}
	public List<Metric> getMetrics() {
		return Metrics;
	}
	public void setMetrics(List<Metric> Metrics) {
		this.Metrics = Metrics;
	}
}
