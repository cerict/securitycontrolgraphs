package eu.musaproject.multicloud.models.macmSLA;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.graphs.graphModel;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class macmSLA extends graphModel{
	String itsName;
	String itsControlFramework;
	String slaid;
	public String getSlaid() {
		return slaid;
	}

	public void setSlaid(String slaid) {
		this.slaid = slaid;
		for(Map.Entry<String, Peer> entry: Peers.entrySet()) {
			entry.getValue().setProperty("slaid", slaid);
		}
	}

	int appid;
	public int getAppid() {
		return appid;
	}

	public void setAppid(int appid) {
		this.appid = appid;
		Peer p=Peers.get(itsName);
		if (p!=null) {
			p.setProperty("app_id", ""+appid);
		}
	}

	SLAPeerType type=SLAPeerType.SLA;

	SLAPeerType getType() {return type;}

	public String SetName(String Name) {
		itsName=Name;
		this.addPeer(itsName, type);
		return itsName;
	}

	public macmSLA(String name, String CF) {
		super();
		this.itsName=name;
		this.itsControlFramework=CF;
		this.addPeer(itsName, type);
		appid=0;
		slaid="x";
	}

	public macmSLA(String name){
		super();
		this.itsName=name;
		this.itsControlFramework="NIST_Framework";
		this.addPeer(itsName, type);		
		appid=0;
		slaid="x";
	}

	public macmSLA(String name, int appid) {
		super();
		this.itsName=name;
		this.itsControlFramework="NIST_Framework";
		this.addPeer(itsName, type);
		this.appid=appid;
		slaid="x";
	}
	public macmSLA() {
		super();
		appid=0;
		slaid="x";
	}

	public String getName() {
		return itsName;
	}

	public boolean hasControl(String SC) {
		return Peers.containsKey(SC);
	}
	public Peer addSecurityControl(String name){
		Peer start,stop;
		String family=StringUtils.substringBefore(name, "_").toUpperCase();
		if (!Peers.containsKey(name.toUpperCase())) {
			//		System.out.println("adding "+name.toUpperCase()+" and rel to "+family);
			start=addPeer(name.toUpperCase(),SLAPeerType.SecurityControl);
			stop=getPeers().get(family);
			if (stop==null) {
				//			System.out.println("Adding family");
				stop=addControlFamily(family);
			}
			Relationship rel= new Relationship(start,stop,SLARelationshipType.SecurityControlOf);
			Rels.add(rel);
			if(rel.endNode==null) {
				System.out.println("Error in rel");
				System.exit(-1);
			}
		} else {
			start=Peers.get(name.toUpperCase());
//			System.out.println("Control already in SLA..");
		}
		start.setProperty("slaid", slaid);
		return start;
	}

	public Peer addControlFamily(String name){
		//		System.out.println("Add CF "+name);
		Peer p=addPeer(name,SLAPeerType.ControlFamily);
		addControlFamilyIn(name,itsName);
		p.setProperty("slaid", slaid);
		return p;
	};

	public Peer addSLO(String name){	
		Peer tmp=addPeer(name,SLAPeerType.SLO);
		tmp.setProperty("slaid", slaid);
		return tmp;
	};

	public Peer addMetric(String name){
		Peer tmp=addPeer(name,SLAPeerType.SecurityMetric);
		tmp.setProperty("slaid", slaid);
		return tmp;
	};

	public Relationship addSecurityControlOf(String SC, String CF) {
		return this.addRelationship(SC, CF, SLARelationshipType.SecurityControlOf);
	}
	public Relationship addControlFamilyIn(String CF, String SLAname) {
		return this.addRelationship(CF, SLAname, SLARelationshipType.ControlFamilyIn);
	}

	public Relationship addSLOin(String SC, String CF) {
		return this.addRelationship(SC, CF, SLARelationshipType.SLOin);
	}
	public Relationship addMeasuredWith(String SC, String CF) {
		return this.addRelationship(SC, CF, SLARelationshipType.MeasuredWith);
	}

	public Relationship addMappedTo(String SC, String CF) {
		return this.addRelationship(SC, CF, SLARelationshipType.MappedTo);
	}

	public void printSecurityControls() {
		System.out.println("**SLA:"+itsName);
		for (Map.Entry<String, Peer> entry: Peers.entrySet()) {
			if(entry.getValue().getType()==SLAPeerType.SecurityControl) {
				System.out.print(" "+entry.getValue().getName()+",");
			}
		}
		System.out.println("*****");
	}
	public void readNeo(String SLAname) {
        Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(NeoDriver.neoUsername, NeoDriver.neoPassword));

		Session session = driver.session();

		String statement="";

		this.itsName=SLAname;
		this.addPeer(itsName, this.getType());

		//statement ="MATCH (p:SLA {name:'"+SLAname+"}) return p.name AS name ";

		//match with slaid
		String slaidsearch="";
		if (!(slaid.equals("x"))) slaidsearch=",slaid:'"+slaid+"'";

		//match with appid
		String appidsearch="";
		if (appid!=0) appidsearch=",app_id:'"+appid+"'";
		
		//Add All families in the sla
		statement ="match (n:"+this.getType()+" {name:'"+SLAname+"'"+slaidsearch+appidsearch+"}), (cf)-[:ControlFamilyIn] ->(n) return cf.name AS name";
				System.out.println(statement);
		StatementResult result=session.run(statement);
		while ( result.hasNext() )
		{
			Record record = result.next();
			String cf=record.get("name").asString();
			addControlFamily(cf);
			//addControlFamilyIn(cf,itsName);

			statement ="match (n:"+this.getType()+" {name:'"+SLAname+"'"+slaidsearch+appidsearch+"}), (cf {name:'"+cf+"'})-[:ControlFamilyIn] ->(n), (c)-[:SecurityControlOf]->(cf) return c.name AS name";
			System.out.println("Getting SLA: "+statement);
			StatementResult result2=session.run(statement);
			while ( result2.hasNext() )
			{
				Record record2 = result2.next();
				String c=record2.get("name").asString();
				addSecurityControl(c);
				//addSecurityControlOf(c,cf);
			}
		}

		//Add SLOs
		statement ="match (n:"+this.getType()+" {name:'"+SLAname+"'"+slaidsearch+appidsearch+"}), (slo)-[:SLOin] ->(n) return slo.name AS name";
		StatementResult result3=session.run(statement);

		while ( result3.hasNext() )
		{
			Record record = result3.next();
			String slo=record.get("name").asString();
			addSLO(slo);
			addSLOin(slo,itsName);

			statement ="match (n:"+this.getType()+" {name:'"+SLAname+"'"+slaidsearch+appidsearch+"}), (slo {name:'"+slo+"'})-[:SLOin] ->(n), (slo)-[:MeasuredWith]->(sm) return sm.name AS name";
			StatementResult result4=session.run(statement);
			while ( result4.hasNext() )
			{
				Record record2 = result4.next();
				String sm=record2.get("name").asString();
				addMetric(sm);
				addMeasuredWith(slo,sm);

				statement ="match (n:"+this.getType()+" {name:'"+SLAname+"'"+slaidsearch+appidsearch+"}), (slo {name:'"+slo+"'})-[:SLOin] ->(n), (slo)-[:MeasuredWith]->(sm {name:'"+sm+"'}), (sm) -[:Mappedto]->(c) return c.name AS control";
				StatementResult result5=session.run(statement);
				System.out.println(statement);
				while ( result5.hasNext() )
				{
					Record record3 = result5.next();
					String c=record3.get("control").asString();
					addMappedTo(sm,c);
				}
			}
		}


		session.close();
		driver.close();


	}

	static public void main(String args[]){
		macmSLA sla=new macmSLA();
		sla.readNeo("csla");
		sla.print();
	}
}
