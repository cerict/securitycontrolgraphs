package eu.musaproject.multicloud.models.macmSLA;

public class macmSLAT extends macmSLA {
	SLAPeerType type=SLAPeerType.SLAT;
	SLAPeerType getType() {return type;}
	
	public macmSLAT() {
		super();
	}
	public macmSLAT(String name, int appid) {
		super(name,appid);
	}

}
