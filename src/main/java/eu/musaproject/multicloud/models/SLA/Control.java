package eu.musaproject.multicloud.models.SLA;

import java.util.HashMap;
import java.util.Map;

public class Control {
	ControlLevel type;
	String name;
	Map<String,String> properties;
	
	public Control() {
		properties=new HashMap<String,String>();
	}
	
	public Control(Map<String,String> props) {
		properties=props;
	}

	public Control(String n, ControlLevel t) {
		type=t;
		name=n;
		Map<String,String> properties;
	}
	
	public String getName() {
		return name;
	}
	
	public ControlLevel getType() {
		return type;
	}
}
