package eu.musaproject.multicloud.models.SLA;

import java.util.HashMap;
import java.util.Map;

public class Link {
	Control startNode;
	Control endNode;
	LinkType type;
	Map<String,String> properties;
	
	public Link() {
		properties=new HashMap<String,String>();
	}
	
	public Link(Map<String,String> props) {
		properties=props;
	}
	
	public Link(Control start,Control end, LinkType t) {
		startNode=start;
		endNode=end;
		type=t;
		properties=new HashMap<String,String>();
	}
	
	public LinkType getType() {
		return type;
	}
	
	public Control getStartNode() {
		return startNode;
	}
	
	public Control getEndNode() {
		return endNode;
	}

}
