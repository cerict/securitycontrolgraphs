package eu.musaproject.multicloud.models.graphs;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Peer {
	public TypeInterface type;
	String name;
	Properties properties;
//	Map<String,String> properties;
	
	public Peer() {
		properties= new Properties();
//		properties=new HashMap<String,String>();
	}
	
//	public Peer(Map<String,String> props) {
//		properties=props;
//	}

	public Peer(Properties p) {
		properties=p;
	}
	
	public Peer(String n, TypeInterface t) {
		type=t;
		name=n;
		properties= new Properties();
	}
	
	public String getName() {
		return name;
	}
	
	public TypeInterface getType() {
		return type;
	}
	
	public Properties getProperties() {
		return properties;
	}
	
	public void setProperties(Properties p) {
		properties=p;
	}
	
	public void setProperty(String key, String value) {
		properties.put(key, value);
	}
	
	public String getProperty(String key) {
		return properties.getProperty(key);
	}
	
}
