package eu.musaproject.slagenerator.agreementoffermodel;

/**
 * Created by adrian on 11.01.2016.
 */

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="CCMSecurityControl" type="{http://www.specs-project.eu/resources/schemas/xml/SLAtemplate}NISTcontrol" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="frameworkName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "controlFramework", propOrder = {
        "id", "frameworkName", "securityControl"
})
@XmlRootElement(name = "controlFramework")
public class ControlFramework implements Serializable {
    @XmlElementRefs(
            {@XmlElementRef(name = "securityControl", namespace = "http://specs-project.eu/SLAtemplate", type = NISTSecurityControl.class),
             @XmlElementRef(name = "securityControl", namespace = "http://specs-project.eu/SLAtemplate", type = CCMSecurityControl.class)
            }
    )
    protected List<AbstractSecurityControl> securityControl;

    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "frameworkName", required = true)
    protected String frameworkName;

    public String getFrameworkName() {
        return frameworkName;
    }

    public void setFrameworkName(String frameworkName) {
        this.frameworkName = frameworkName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<AbstractSecurityControl> getSecurityControl() {
        return securityControl;
    }

    public void setSecurityControl(List<AbstractSecurityControl> securityControl) {
        this.securityControl = securityControl;
    }

}
