package eu.musaproject.slagenerator.agreementoffermodel;

import javax.xml.bind.annotation.*;
import java.io.Serializable;


/**
 * <p>Java class for capabilityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="capabilityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="control_framework">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence maxOccurs="unbounded">
 *                   &lt;element name="CCMSecurityControl" type="{http://www.specs-project.eu/resources/schemas/xml/SLAtemplate}NISTcontrol" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="frameworkName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="descr" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "capability", namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate", propOrder = {
    "controlFramework"
})
@XmlRootElement(namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate", name = "capability")
public class CapabilityType implements Serializable {

    private static final long serialVersionUID = -6236713231506451921L;
    @XmlElement(name = "controlFramework", namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate", required = true)
    protected ControlFramework controlFramework;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "description", required = true)
    protected String description;

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    @XmlAttribute(name = "mandatory", required = true)
    protected boolean mandatory;


    /**
     * Gets the value of the controlFramework property.
     * 
     * @return
     *     possible object is
     *     {@link ControlFramework }
     *     
     */
    public ControlFramework getControlFramework() {
        return controlFramework;
    }

    /**
     * Sets the value of the controlFramework property.
     * 
     * @param value
     *     allowed object is
     *     {@link ControlFramework }
     *     
     */
    public void setControlFramework(ControlFramework value) {
        this.controlFramework = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the descr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the descr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }



}
