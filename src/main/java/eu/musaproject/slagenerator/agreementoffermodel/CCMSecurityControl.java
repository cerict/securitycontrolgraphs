
package eu.musaproject.slagenerator.agreementoffermodel;

import javax.xml.bind.annotation.*;
import java.io.Serializable;


/**
 * <p>Java class for security_controlType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="security_controlType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="control_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="control_domain" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="security_control" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CCMsecurityControl", propOrder = {"id", "name", "controlDomain", "controlDescription", "importanceWeight"
})
@XmlRootElement(name = "CCMsecurityControl", namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate")
public class CCMSecurityControl extends AbstractSecurityControl implements Serializable{

    private static final long serialVersionUID = 4912007320478742026L;
    @XmlAttribute(name = "id", required = true)//,namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm")
    protected String id;
    @XmlAttribute(name = "name", required = true)//, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm")
    protected String name;
    @XmlAttribute(name = "control_domain", required = true)//, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm")
    protected String controlDomain;
//    @XmlAttribute(name = "security_control", required = true, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm")
//    protected String securityControl;
//    @XmlAttribute(name = "control_enhancement", required = true, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm")
//    protected String controlEnhancement;
    @XmlElement(name = "description", namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm", required = true)
    protected String controlDescription;
    @XmlElement(name = "importance_weight", namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm", required = true)
    @XmlSchemaType(name = "string")
    protected WeightType importanceWeight;


    public String getControlDescription() {
        return controlDescription;
    }

    public void setControlDescription(String controlDescription) {
        this.controlDescription = controlDescription;
    }

    public WeightType getImportanceWeight() {
        return importanceWeight;
    }

    public void setImportanceWeight(WeightType importanceWeight) {
        this.importanceWeight = importanceWeight;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getControlDomain() {
        return controlDomain;
    }

    public void setControlDomain(String controlDomain) {
        this.controlDomain = controlDomain;
    }

//    public String getSecurityControl() {
//        return securityControl;
//    }
//
//    public void setSecurityControl(String securityControl) {
//        this.securityControl = securityControl;
//    }
//
//    public String getControlEnhancement() {
//        return controlEnhancement;
//    }

//    public void setControlEnhancement(String controlEnhancement) {
//        this.controlEnhancement = controlEnhancement;
//    }

    public String toString(){
        return "CCMSC[" + id + "," + controlDomain + "," + /*securityControl +*/ "," + controlDescription +  "]";
    }

}
