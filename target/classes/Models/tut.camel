camel model TSMModel {
location model TSMLocation {
	region EU {
		name: Europe 
	}

	country DE {
		name: Germany
		// parentRegions is not a compulsory property 
		// parent regions [TSMLocation.EU]
	}

	country UK {
		name: UnitedKingdom
		parent regions [TSMLocation.EU]
	}

	country FI {
		name: Finland
		parent regions [TSMLocation.EU]
	}

} 

requirement model TSMRequirement {
	// example of QuantitativeHardwareRequirement only applicable to a specific Component 
	quantitative hardware JourneyPlanner {
		ram: 1024..   // always in MEGABYTES
		storage: 10.. // always in GIGABYTES // TODO: Is it required to link with range values and units???
	}

	/**TUT: maybe 32 cores is a little bit too much if we are talking about microservices **/
	quantitative hardware CoreIntensive {
		core: 4..16
		ram: 1024..8192
	}

	quantitative hardware CPUIntensive {
		core: 1..       // min and max number of CPU cores
		ram: 1024..8192 // size of RAM 
		cpu: 1.0..      // min and max CPU frequency
	}

	quantitative hardware StorageIntensive {
		storage: 1000..
	}

	os Ubuntu {os: Ubuntu 64os}

	location requirement GermanyReq {
		locations [TSMLocation.DE]
	}

	location requirement UKReq {
		locations [TSMLocation.UK]
	}
	
	location requirement FinlandReq {
		locations [TSMLocation.FI]
	}
	
	horizontal scale requirement HorizontalScaleTSMEngineHAFw {
	component: TSMModel.TSMDeployment.TSMEngineHAFw
	instances: 1..5
	}
 
    /* TODO MUSA
	//Is this really needed?
	vertical scale requirement VerticalScaleTSMEngine {
	vm: TSMModel.TSMDeployment.CPUIntensiveUbuntuFinland 
	CPU: 1.0..24.0
	}
	*/

} // requirement model TSMRequirement

scalability model TSMScalability {
//model all scalability requirements for TSM app

////horizontal sale rules.

	horizontal scaling action HorizontalScalingTSMEngineHAFw {
		type: SCALE OUT
		vm: TSMModel.TSMDeployment.CPUIntensiveUbuntuFinland
		internal component: TSMModel.TSMDeployment.TSMEngineHAFw
		/**TUT: Command how to use the SCALE OUT action **/
		//MCP command: pcs resource $NAME clone-max=$NUM
		/**TUT: Should it be provided here or somewhere else? **/
		/**TUT: $NAME is static and related to the component **/
		/**TUT: $NUM is dynamic and most likely should be taken from runtime config **/
	}

	non-functional event CPUAvgMetricNFEAll {
		metric condition: TSMModel.TSMMetric.CPUAvgMetricConditionAll
		violation
	}

	non-functional event CPUAvgMetricNFEAny {
		metric condition: TSMModel.TSMMetric.CPUAvgMetricConditionAny
		violation
	}

	binary event pattern CPUAvgMetricBEPAnd {
		left event: TSMModel.TSMScalability.CPUAvgMetricNFEAll
		right event: TSMModel.TSMScalability.CPUAvgMetricNFEAny
		operator: AND
	}

	scalability rule RawCPUScalabilityRule {
		event: TSMModel.TSMScalability.CPUAvgMetricBEPAnd
		actions [TSMModel.TSMScalability.HorizontalScalingTSMEngineHAFw]
		scale requirements [TSMRequirement.HorizontalScaleTSMEngineHAFw]
	}
}
////vertical scale rules. Pending

scalability model TSMScalability {
//model all scalability requirements for TSM app

////horizontal sale rules.

	horizontal scaling action HorizontalScalingTSMEngineHAFw {
		type: SCALE OUT
		vm: TSMModel.TSMDeployment.CPUIntensiveUbuntuFinland
		internal component: TSMModel.TSMDeployment.TSMEngineHAFw
		/**TUT: Command how to use the SCALE OUT action **/
		//MCP command: pcs resource $NAME clone-max=$NUM
		/**TUT: Should it be provided here or somewhere else? **/
		/**TUT: $NAME is static and related to the component **/
		/**TUT: $NUM is dynamic and most likely should be taken from runtime config **/
	}

	non-functional event CPUAvgMetricNFEAll {
		metric condition: TSMModel.TSMMetric.CPUAvgMetricConditionAll
		violation
	}

	non-functional event CPUAvgMetricNFEAny {
		metric condition: TSMModel.TSMMetric.CPUAvgMetricConditionAny
		violation
	}

	binary event pattern CPUAvgMetricBEPAnd {
		left event: TSMModel.TSMScalability.CPUAvgMetricNFEAll
		right event: TSMModel.TSMScalability.CPUAvgMetricNFEAny
		operator: AND
	}

	scalability rule RawCPUScalabilityRule {
		event: TSMModel.TSMScalability.CPUAvgMetricBEPAnd
		actions [TSMModel.TSMScalability.HorizontalScalingTSMEngineHAFw]
		scale requirements [TSMRequirement.HorizontalScaleTSMEngineHAFw]
	}

////vertical scale rules. Pending

} // scalability model TSMScalability

type model TUTType {
	enumeration VMTypeEnum {
		values [ 'M1.MICRO' : 0,
		'M1.TINY' : 1,
		'M1.SMALL' : 2,
		'M1.MEDIUM' : 3,
		'M1.LARGE' : 4,
		'M1.XLARGE' : 5,
		'M1.XXLARGE' : 6,
		'M2.SMALL' : 7,
		'M2.MEDIUM' : 8,
		'M2.LARGE' : 9,
		'M2.XLARGE' : 10,
		'C1.SMALL' : 11,
		'C1.MEDIUM' : 12,
		'C1.LARGE' : 13,
		'C1.XLARGE' : 14,
		'C1.XXLARGE' : 15 ]
	}
	range MemoryRange {
		primitive type: IntType
		lower limit {
			int value 256 included
		}
		upper limit {
			int value 32768 included
		}
	}
	range StorageRange {
		primitive type: IntType
		lower limit {
			int value 0 included
		}
		upper limit {
			int value 160 included
		}
	}
	range CoresRange {
		primitive type: IntType
		lower limit {
			int value 1 included
		}
		upper limit {
			int value 16 included
		}
	}
	string value type StringValueType {
		primitive type: StringType
	}
	
	list StorageList {
		values [ int value 0,
		int value 20,
		int value 40,
		int value 80,
		int value 160 ]
	}
	
	list MemoryList {
		values [ int value 256,
		int value 512,
		int value 2048,
		int value 4096,
		int value 8192,
		int value 16384,
		int value 32768 ]
	}
	
	list CoresList {
		values [ int value 1,
		int value 2,
		int value 4,
		int value 8,
		int value 16 ]
	}
	
	range Range_0_100 {
		primitive type: IntType
		lower limit {int value 0 included}
		upper limit {int value 100}
	}
	
	range Range_0_10000 {
		primitive type: IntType
		lower limit {int value 0}
		upper limit {int value 10000 included}
	}
	
	range DoubleRange_0_100 {
		primitive type: DoubleType
		lower limit {double value 0.0 included}
		upper limit {double value 100.0 included}
	}	
} // type model TUTType

unit model TSMUnit {
	storage unit { StorageUnit: GIGABYTES }
	
	time interval unit {minutes: MINUTES}
	
	time interval unit {seconds: SECONDS}
	
	/* 
	 * TODO: Is it required in MUSA??? 
	memory unit { MemoryUnit: MEGABYTES }
	*/
	
	/* some examples... */
    monetary unit {Euro: EUROS}
	throughput unit {SimulationsPerSecondUnit: TRANSACTIONS_PER_SECOND}
	time interval unit {ResponseTimeUnit: MILLISECONDS}
	time interval unit {ExperimentMakespanInSecondsUnit: SECONDS}
	transaction unit {NumberOfSimulationsLeftInExperimentUnit: TRANSACTIONS}
	dimensionless {AvailabilityUnit: PERCENTAGE}
	dimensionless {CPUUnit: PERCENTAGE}
} // unit model TUTUnit


application TSMApplication {
	version: 'v1.0'
	owner: TUTOrganisation.test_user1
	deployment models [TSMModel.TSMDeployment]
} // application TSMApplication



organisation model TUTOrganisation {
	organisation TUT {
		www: 'http://www.tut.fi/en/home'
		postal address: 'Korkeakoulunkatu 10, 33720 Tampere, Finland'
		email: 'test.test@tut.fi'
	}

	user test_user1 {
		first name: test_name
		last name: test_surname
		email: 'test_name.test_surname@tut.fi'
		musa credentials {
		    end time: 2017-11-01
			username: 'mcp'
			password: 'test_name_surname'
			}
	}
	
	user test_user2 {
	first name: user2_name
	last name: user2_surname
	email: 'its email'
	musa credentials {
	    username: 'user2'
	    password: 'user2_passw'
	    }
	}
	
	user group test_group {
	users [TUTOrganisation.test_user1, TUTOrganisation.test_user2]
	}
	
	
	role devop
	
    role assignment test_nameDevop {
        start: 2016-02-26
        end: 2017-02-26
        assigned on: 2016-02-25
        users: [TUTOrganisation.test_user1, TUTOrganisation.test_user2]
        role: devop
    }
    
    role assignment test_groupDevop {
        start: 2016-02-01
        end: 2017-02-26
        assigned on:  2016-02-25
        role: devop
        user groups: [TUTOrganisation.test_group]
    }

	security level: HIGH
} // organisation model TUTOrganisation


deployment model TSMDeployment {
// example of VMRequirementSet only applicable to a specific Component 
	requirement set JourneyPlannerHostRS {
		os: TSMRequirement.Ubuntu
		quantitative hardware: TSMRequirement.JourneyPlanner
		location: TSMRequirement.UKReq
	}

	requirement set CoreIntensiveUbuntuFinlandRS {
		os: TSMRequirement.Ubuntu
		quantitative hardware: TSMRequirement.CoreIntensive
		location: TSMRequirement.UKReq
	}

	requirement set CPUIntensiveUbuntuFinlandRS {
		os: TSMRequirement.Ubuntu
		quantitative hardware: TSMRequirement.CPUIntensive
		location: TSMRequirement.UKReq
	}

	requirement set CPUIntensiveUbuntuUKRS {
		os: TSMRequirement.Ubuntu
		quantitative hardware: TSMRequirement.CPUIntensive
		location: TSMRequirement.UKReq
	}
	
	requirement set StorageIntensiveUbuntuFinlandRS {
		os: TSMRequirement.Ubuntu
		quantitative hardware: TSMRequirement.StorageIntensive
		location: TSMRequirement.UKReq
	}


	vm JourneyPlanner {
		requirement set JourneyPlannerHostRS
		provided host JourneyPlannerHost
	}


	vm CoreIntensiveUbuntuFinland {
		requirement set CoreIntensiveUbuntuFinlandRS
		provided host CoreIntensiveUbuntuFinlandHost
	}

	vm CPUIntensiveUbuntuFinland {
		requirement set CPUIntensiveUbuntuFinlandRS
		provided host CPUIntensiveUbuntuFinlandHost
	}

	vm StorageIntensiveUbuntuFinland {
		requirement set StorageIntensiveUbuntuFinlandRS
		provided host StorageIntensiveUbuntuFinlandHost
	}

	vm CPUIntensiveUbuntuUK {
		requirement set CPUIntensiveUbuntuUKRS
		provided host CPUIntensiveUbuntuUKHost
	}


	internal component TSMEngine {
		type: MUSA.MUSADEP.ENFAG3
		context path: 'tsme'

		provided communication TSMEnginePort { port: 8185 ports range: 0..32800  context path: 'tsme' }
		provided communication TSMEngineRESTPort { port: 445 ports range: 32768..32800 context path: 'tsme' }
				
		required communication IDManagerPortReq {port: 8080 ports range: 32768..32800 context path: 'tsme' mandatory}
		required communication JourneyPlannerPortReq {port: 8085  ports range: 32768..32800 context path: 'tsme2'  mandatory}
		required communication DatabasePortReq {port: 3306 ports range: 32768..32800 context path: 'tsme' mandatory}

		
		provided communication TSMEnginePort { port: 8185 }
		provided communication TSMEngineRESTPort { port: 445 }
				
		required communication IDManagerPortReq {port: 8080 mandatory}
		required communication JourneyPlannerPortReq {port: 8085  mandatory}
		required communication DatabasePortReq {port: 3306 mandatory}
		
		required host CoreIntensiveUbuntuFinlandHostReq

		
		/**************
		 ** One of two possible configurations will be used
		 **************/

		configuration TSMEngineConfiguration {
			download: 'wget https://github.com/musa/TSM_images/tsme.tar'
			install: 'sudo apt-get install docker.io && systemctl enable docker && systemctl start docker && docker load < tsme.tar'
			start: 'docker run -it -d fi.tut.fast.musa/tsme --mmt-endpoint=1.2.3.4 --cec-endpoint=5.6.7.8'
			}
			
		configuration TSMEngineConfigurationCHEF {
			CHEF configuration manager C1 { //Configuration Management tool
				cookbook: 'TSMEgineAccessControlAgentConfigurationCookbook1'
				recipe: 'RecipeZ'
			}
		}			
	}


	internal component TSMEgineAccessControlAgent {//MUSA Enforcement Agent from a list in the Catalogue
		//type MUSA Enforcement component
		type: MUSA.MUSADEP.ENFAG2
	
		provided communication TSMEgineAccessControlAgentPort{port: 000}
		required communication TSMEgineAccessControlAgentReq{port: 000 mandatory}
		
		required host CoreIntensiveUbuntuFinlandHostReq /*The same host as TSMEngine or other.*/

		configuration TSMEgineAccessControlAgentConfiguration {
			CHEF configuration manager C1 { //Configuration Management tool
				cookbook: 'TSMEgineAccessControlAgentConfigurationCookbook1'
				recipe: 'RecipeZ'
			}
		}
	}

	internal component TSMEgineVulnScannerAgent {
		type: MUSA.MUSADEP.ENFAG1
		provided communication TSMEgineVulnScannerAgentPort{port: 000 }
		required communication TSMEgineVulnScannerAgentReq{port: 000 mandatory}
		
		required host CoreIntensiveUbuntuFinlandHostReq //The same host as the mc app internial component associated to it, TSMEngine.
		 
		
		configuration TSMEgineAccessControlAgentConfiguration{
			CHEF configuration manager C1 { //Configuration Management tool
				cookbook: 'TSMEgineAccessControlAgentConfigurationCookbook1'
				recipe: 'RecipeZ'
			}
		}
	}


	/**TUT: probably the correct way to describe the dynamic routing **/
	/**TUT: including not only HA framework, but any load-balancing proxy with dynamic config **/
	internal component TSMEngineHAFw {
	    /**TUT: In case of HAfw, there are two components, outbound balancer and inbound GW **/
		/**TUT: it might be a good idea to separate those logically within the model **/
		/**TUT: outbound balancer's ports are mostly fixed (or, rather, user-defined) **/
		/**TUT: inbound GW ports are randomly assigned by Docker from the given range. **/
		/**TUT: The basic question is to whom (service-wise/address-wise) the provided port is provided, **/ 
		/**TUT: as it can listen to different IP ranges, including just localhost. **/
		provided communication TSMEngineHAFwPort{port: 000 }
		required communication TSMEngineHAFwPort{port: 000 mandatory}
		//??? 
		required host CoreIntensiveUbuntuFinlandHostReq /*The same host as TSMEngine or other.*/
		
		configuration TSMEngineHAFwConfiguration{
			CHEF configuration manager C1 { //Configuration Management tool
				cookbook: 'TSMEngineHAFwConfigurationCookbook1'
				recipe: 'RecipeZ'
			}
		}
	}




	internal component JourneyPlanner {
		provided communication JourneyPlannerPort {port: 8085}
		
		required communication ConsumptionEstimatorPortReq {port: 9090 mandatory}
		
		required communication GoogleDirectionsPortReq { port: 9999 mandatory} //????
		required communication GoogleMapsPortReq { port: 9999 mandatory} //????
		required communication ITSFactoryPortReq { port: 9999 mandatory} //????
		required communication FMIPortReq { port: 9999 mandatory} //????		
		
		//required host CPUIntensiveUbuntuFinlandHostReq
		required host JourneyPlannerHostReq

		configuration JourneyPlannerManualConfiguration{
			download: 'wget https://github.com/musa/TSM_service_scripts/archive/musa.tar.gz && sudo apt-get update && sudo apt-get install -y groovy ant && tar -zxvf musa.tar.gz && cd TSM_service_scripts-musa'
			install: 'cd TSM_service_scripts-musa && ./information_service_install.sh'
			start: 'cd TSM_service_scripts-musa && ./information_service_start.sh'
		}
	}

	internal component ConsumptionEstimator {
		provided communication ConsumptionEstimatorPort {port: 9090}
		
		required communication HAFrameworkPortCECPortReq {}
		
		required host CPUIntensiveUbuntuUKHostReq

		configuration ConsumptionManualEstimatorConfiguration{
			download: 'wget https://github.com/musa/TSM_service_scripts/archive/musa.tar.gz && sudo apt-get update && sudo apt-get install -y groovy ant && tar -zxvf musa.tar.gz && cd TSM_service_scripts-musa'
			install: 'cd TSM_service_scripts-musa && ./simulation_manager_install.sh'
			start: 'cd TSM_service_scripts-musa && ./simulation_manager_start.sh'
		}
	}

	/**TUT: naming **/
	internal component IDMAM {
		provided communication IDManagerPort {port: 8080}

		required host CoreIntensiveUbuntuFinlandHostReq

		configuration IDManagerManualConfiguration{
			download: 'wget https://github.com/musa/TSM_service_scripts/archive/musa.tar.gz && sudo apt-get update && sudo apt-get install -y groovy ant && tar -zxvf musa.tar.gz && cd TSM_service_scripts-musa'
			install: 'cd TSM_service_scripts-musa && ./storage_manager_install.sh'
			start: 'cd TSM_service_scripts-musa && ./storage_manager_start.sh'
		}
	}
	
	/**TUT: there are two databases, one for IDM (Mongo), and one for TSM engine (Postgres+PostGIS) **/
	internal component IDMAMDatabase {
		provided communication DatabasePort {port: 3306}
		
		required host StorageIntensiveUbuntuFinlandHostReq

		configuration DatabaseManualConfiguration{
			download: 'wget https://github.com/musa/TSM_service_scripts/archive/musa.tar.gz && sudo apt-get update && sudo apt-get install -y groovy ant && tar -zxvf musa.tar.gz && cd TSM_service_scripts-musa'
			install: 'cd TSM_service_scripts-musa && ./storage_manager_install.sh'
			start: 'cd TSM_service_scripts-musa && ./storage_manager_start.sh'
		}
	}
	
	internal component JourneyDatabase {
		provided communication DatabasePort {port: 3306}
		
		required host StorageIntensiveUbuntuFinlandHostReq

		configuration DatabaseManualConfiguration{
			download: 'wget https://github.com/musa/TSM_service_scripts/archive/musa.tar.gz && sudo apt-get update && sudo apt-get install -y groovy ant && tar -zxvf musa.tar.gz && cd TSM_service_scripts-musa'
			install: 'cd TSM_service_scripts-musa && ./storage_manager_install.sh'
			start: 'cd TSM_service_scripts-musa && ./storage_manager_start.sh'
		}
	}
	
	hosting TSMEngineToCoreIntensiveUbuntuFinlandHost {
		from TSMEngine.CoreIntensiveUbuntuFinlandHostReq to CoreIntensiveUbuntuFinland.CoreIntensiveUbuntuFinlandHost
	}

    /* 
	hosting JourneyPlannerToCPUIntensiveUbuntuFinland {
		from JourneyPlanner.CPUIntensiveUbuntuFinlandHostReq to CPUIntensiveUbuntuFinland.CPUIntensiveUbuntuFinlandHost
	}
	* 
	*/

	hosting JourneyPlannerToSpecificJourneyPlannerHost {
		from JourneyPlanner.JourneyPlannerHostReq to JourneyPlanner.JourneyPlannerHost
	}

	hosting ConsumptionEstimatorToCPUIntensiveUbuntuUK {
		from ConsumptionEstimator.CPUIntensiveUbuntuUKHostReq to CPUIntensiveUbuntuUK.CPUIntensiveUbuntuUKHost
	}
	
	hosting IDManagerToCoreIntensiveUbuntuFinland {
		from IDMAM.CoreIntensiveUbuntuFinlandHostReq to CoreIntensiveUbuntuFinland.CoreIntensiveUbuntuFinlandHost
	}

	hosting DatabaseToStorageIntensiveUbuntuFinland {
		from IDMAMDatabase.StorageIntensiveUbuntuFinlandHostReq to StorageIntensiveUbuntuFinland.StorageIntensiveUbuntuFinlandHost
	}

	communication TSMEngineToDatabase {
		type: REMOTE
		from TSMEngine.DatabasePortReq to IDMAMDatabase.DatabasePort
		protocol MYSQL
	}

	communication TSMEngineToIDManager {
		type: REMOTE		
		from TSMEngine.IDManagerPortReq to IDMAM.IDManagerPort
	}

	communication TSMEngineToJourneyPlanner {
		type: REMOTE
		from TSMEngine.JourneyPlannerPortReq to JourneyPlanner.JourneyPlannerPort
	}

	communication JourneyPlannerToConsumptionEstimator {
		type: REMOTE		
		from JourneyPlanner.ConsumptionEstimatorPortReq to ConsumptionEstimator.ConsumptionEstimatorPort
	}
} // deployment model TSMDeployment





//Metric model for TSM App
metric model TSMMetric {
	window Win5Min {
		window type: SLIDING
		size type: TIME_ONLY
		time size: 5
		unit: TSMModel.TSMUnit.minutes
	}

	window Win1Min {
		window type: SLIDING
		size type: TIME_ONLY
		time size: 1
		unit: TSMModel.TSMUnit.minutes
	}

	schedule Schedule1Min {
		type: FIXED_RATE
		interval: 1
		unit: TSMModel.TSMUnit.minutes
	}

	schedule Schedule1Sec {
		type: FIXED_RATE
		interval: 1
		unit: TSMModel.TSMUnit.seconds
	}

	property AvailabilityProperty {
		type: MEASURABLE
		sensors [TSMMetric.AvailabilitySensor]
	}

	property CPUProperty {
		type: MEASURABLE
		sensors [TSMMetric.CPUSensor]
	}

	property ResponseTimeProperty {
		type: MEASURABLE
		sensors [TSMMetric.ResponseTimeSensor]
	}

	property FrequencyOfVulnerabilityScanningProperty {
		type: MEASURABLE
		sensors [TSMMetric.FreqOfVulnScanSensor]
	}

	sensor AvailabilitySensor {
		configuration: 'MMTAgent.Availability'
		push
	}

	sensor CPUSensor {
		configuration: 'MMTAgent.CPU'
		push
	}

	sensor ResponseTimeSensor {
		push
	}

	sensor FreqOfVulnScanSensor {
		configuration: 'MMTAgent.FreqOfVulnScan'
		push
	}
	raw metric AvailabilityMetric {
		value direction: 1
		layer: SaaS
		property: TSMModel.TSMMetric.AvailabilityProperty
		unit: TSMModel.TSMUnit.AvailabilityUnit
		value type: TSMModel.TUTType.DoubleRange_0_100
	}

	raw metric CPUMetric {
		value direction: 0
		layer: IaaS
		property: TSMModel.TSMMetric.CPUProperty
		unit: TSMModel.TSMUnit.CPUUnit
		value type: TSMModel.TUTType.Range_0_100
	}

	raw metric ResponseTimeMetric {
		value direction: 0
		layer: SaaS
		property: TSMModel.TSMMetric.ResponseTimeProperty
		unit: TSMModel.TSMUnit.ResponseTimeUnit
		value type: TSMModel.TUTType.Range_0_10000
	}

	composite metric MeanValueOfResponseTimeOfAllTSMEngineMetric {
		value direction: 0
		layer: SaaS
		property: TSMModel.TSMMetric.ResponseTimeProperty
		unit: TSMModel.TSMUnit.ResponseTimeUnit

		metric formula MeanValueOfResponseTimeOfAllTSMEngineFormula {
			function arity: UNARY
			function pattern: MAP
			MEAN(TSMModel.TSMMetric.ResponseTimeMetric)
		}
	}

	composite metric CPUAverage {
		description: "Average usage of the CPU"
		value direction: 1
		layer: PaaS
		property: TSMModel.TSMMetric.CPUProperty
		unit: TSMModel.TSMUnit.CPUUnit

		metric formula Formula_Average {
			function arity: UNARY
			function pattern: REDUCE
			MEAN( TSMModel.TSMMetric.CPUMetric )
		}
	}

	raw metric context TSMEngineAvailabilityContext {
		metric: TSMModel.TSMMetric.AvailabilityMetric
		sensor: TSMMetric.AvailabilitySensor
		component: TSMModel.TSMDeployment.TSMEngine
		quantifier: ANY
	}

	raw metric context CPUMetricConditionContext {
		metric: TSMModel.TSMMetric.CPUMetric
		sensor: TSMMetric.CPUSensor
		component: TSMModel.TSMDeployment.TSMEngine
		quantifier: ANY
	}

	raw metric context TSMEngineResponseTimeContext {
		metric: TSMModel.TSMMetric.ResponseTimeMetric
		sensor: TSMMetric.ResponseTimeSensor
		component: TSMModel.TSMDeployment.TSMEngine
		quantifier: ANY
	}

	raw metric context JourneyPlannerResponseTimeContext {
		metric: TSMModel.TSMMetric.ResponseTimeMetric
		sensor: TSMMetric.ResponseTimeSensor
		component: TSMModel.TSMDeployment.JourneyPlanner
		quantifier: ANY
	}

	raw metric context CPURawMetricContext {
		metric: TSMModel.TSMMetric.CPUMetric
		sensor: TSMMetric.CPUSensor
		component: TSMModel.TSMDeployment.TSMEngine
		schedule: TSMModel.TSMMetric.Schedule1Sec
		quantifier: ALL
	}

	composite metric context CPUAvgMetricContextAll {
		metric: TSMModel.TSMMetric.CPUAverage
		component: TSMModel.TSMDeployment.TSMEngine
		window: TSMModel.TSMMetric.Win5Min
		schedule: TSMModel.TSMMetric.Schedule1Min
		composing metric contexts [TSMModel.TSMMetric.CPURawMetricContext]
		quantifier: ALL
	}

	composite metric context CPUAvgMetricContextAny {
		metric: TSMModel.TSMMetric.CPUAverage
		component: TSMModel.TSMDeployment.TSMEngine
		window: TSMModel.TSMMetric.Win1Min
		schedule: TSMModel.TSMMetric.Schedule1Min
		composing metric contexts [TSMModel.TSMMetric.CPURawMetricContext]
		quantifier: ANY
	}

	metric condition TSMEngineAvailabilityCondition {
		context: TSMModel.TSMMetric.TSMEngineAvailabilityContext
		threshold: 99.0
		comparison operator: >
	}

	metric condition CPUMetricCondition {
		context: TSMModel.TSMMetric.CPUMetricConditionContext
		threshold: 80.0
		comparison operator: >
	}

	metric condition TSMEngineResponseTimeCondition {
		context: TSMModel.TSMMetric.TSMEngineResponseTimeContext
		threshold: 0.3
		comparison operator: <
	}

	metric condition JourneyPlannerResponseTimeCondition {
		context: TSMModel.TSMMetric.JourneyPlannerResponseTimeContext
		threshold: 700.0
		comparison operator: >
	}

	metric condition CPUAvgMetricConditionAll {
		context: TSMModel.TSMMetric.CPUAvgMetricContextAll
		threshold: 50.0
		comparison operator: >
	}

	metric condition CPUAvgMetricConditionAny {
		context: TSMModel.TSMMetric.CPUAvgMetricContextAny
		threshold: 80.0
		comparison operator: >
	}
} // metric model TSMMetric {



}
